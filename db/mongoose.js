var mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/tickets');

module.exports = {mongoose};

// var request = require('request');

// var funcPromise = () => {
// 	return new Promise((resolved, reject) => {
// 		var options = { method: 'GET',
//   		url: 'http://192.168.0.104:3000/ticket_mean',
//   		headers: 
//    		{ 'postman-token': '184b0eea-42cb-1c97-2517-43465fe79c2f',
//      	'cache-control': 'no-cache' } };
// 	});
// 	request(options, function (error, response, body) {
// 		if(error)
// 			reject('unale to connect');
// 		else
// 			resolved(body);
// 	})
// };

// var funcPromise1 = () => {
// 	return new Promise((resolved, reject) => {
// 		var options = { method: 'GET',
//   		url: 'http://192.168.0.104:3000/max_ticket',
//   		headers: 
//    		{ 'postman-token': '184b0eea-42cb-1c97-2517-43465fe79c2f',
//      	'cache-control': 'no-cache' } };
// 	});
// 	request(options, function (error, response, body) {
// 		if(error)
// 			reject('unale to connect');
// 		else
// 			resolved(body);
// 	})
// };


// Promise.all([funcPromise, funcPromise1]).then((msg) => {
// 	console.log(msg);
// }).catch((e) => {
// 	console.log(e);
// })


// funcPromise.then((msg) => {
// 	console.log(msg)
// }, (e) => {
// 	console.log(e);
// });