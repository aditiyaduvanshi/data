var mongoose = require('mongoose');

var TicketPay = mongoose.model('ticket_payments', {
  payment_data: {
    type: String
  },
  payment_type: {
    type: String
  },
  flow: {
    type: Number
  },
  payment_status: {
    type: String
  },
  amount: {
    type: Number
  },
  payment_due_date: {
    type: Date
  },
  payment_details: {
    from: {
      type : String
    },
    to: {
      type: String
    }
  },
  payment_transfer_date: {
    type: Date
  }
},
'ticket_payments');

module.exports = {TicketPay};