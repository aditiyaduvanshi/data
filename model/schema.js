var mongoose = require('mongoose');



var Ticket = mongoose.model('ticket_data', {
  stid: {
    type: Number
  },
  feedback: {
    type: String
  },
  deadline: {
    type: String
  },
  source: {
    type: String
  },
  priority: {
    type: String
  },
  ticket_status: {
    type: String
  },
  managed_home_id: {
    type: String
  },
  last_edit_date: {
    type: String
  },
  status: {
    type: String
  },
  assigned: {
    user_id: {
      type: String
    }
  },
  payment_id_list: [
    {}
  ],
  purpose_description: {
    notes: {
      type: String
    },
    delivery_address: {
      type: String
    }
  },
  item_list: [
    {
      service: {
        type: String
      },
      vendor_id: {
        type: String
      },
      _id: {
        type: String
      }
    }
  ],
  ticket_raised_event: {
    
      time_stamp: {
        type: Date
      },
      user_id: {
        type: String
      }
    
  },
  document_list: [],
  ticket_closed_event: {
    
      time_stamp: {
        type: Date
      },
      user_id: {
        type: String
      }
    
  }
}, 'ticket_data');

module.exports = {Ticket};